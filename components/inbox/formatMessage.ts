import { format, isToday } from "date-fns"

export function formatMessageDate(date: string) {
  // format message date to always include am/pm indicator for all cases
  return isToday(new Date(date))
    ? format(new Date(date), "h:mm a")
    : format(new Date(date), "dd MMM, h:mm a")
}
