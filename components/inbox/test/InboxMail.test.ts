// Import necessary utilities from Vue Test Utils and the function to test
import { describe, it, expect } from "vitest"
import { formatMessageDate } from "../formatMessage"

describe("formatMessageDate", () => {
  it('formats today\'s date with "HH:mm a" format', () => {
    const today = new Date().toISOString().split("T")[0] // Get today's date in YYYY-MM-DD format
    const date = `${today}T14:00:00.000Z` // Example time
    expect(formatMessageDate(date)).toBe("4:00 PM")
  })

  it('formats non-today\'s date with "dd MMM, HH:mm a" format', () => {
    const nonToday = "2024-01-01T14:00:00.000Z" // Example non-today date
    expect(formatMessageDate(nonToday)).toBe("01 Jan, 3:00 PM")
  })
})
